-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: caja
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `caja`
--

DROP TABLE IF EXISTS `caja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `caja` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_dinero` int DEFAULT NULL,
  `cantidad` int NOT NULL,
  `cambio` double NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `caja_id` (`id`),
  KEY `IDX_E465F405C553A278` (`id_dinero`),
  CONSTRAINT `FK_E465F405C553A278` FOREIGN KEY (`id_dinero`) REFERENCES `dinero` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caja`
--

LOCK TABLES `caja` WRITE;
/*!40000 ALTER TABLE `caja` DISABLE KEYS */;
INSERT INTO `caja` VALUES (1,8,5,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(2,7,10,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(3,4,15,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(4,3,20,0,'2021-03-02 17:58:23','2021-03-02 17:58:23');
/*!40000 ALTER TABLE `caja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dinero`
--

DROP TABLE IF EXISTS `dinero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dinero` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_tipo_dinero` int DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `IDX_E36417956ECF2C7A` (`id_tipo_dinero`),
  CONSTRAINT `FK_E36417956ECF2C7A` FOREIGN KEY (`id_tipo_dinero`) REFERENCES `tipo_dinero` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dinero`
--

LOCK TABLES `dinero` WRITE;
/*!40000 ALTER TABLE `dinero` DISABLE KEYS */;
INSERT INTO `dinero` VALUES (1,1,'Cincuenta',50,'2021-03-02 17:57:37','2021-03-02 17:57:37'),(2,1,'Cien',100,'2021-03-02 17:57:37','2021-03-02 17:57:37'),(3,1,'Dosientos',200,'2021-03-02 17:57:37','2021-03-02 17:57:37'),(4,1,'Quinientos',500,'2021-03-02 17:57:37','2021-03-02 17:57:37'),(5,2,'Mil',1000,'2021-03-02 17:57:37','2021-03-02 17:57:37'),(6,2,'Cinco Mil',5000,'2021-03-02 17:57:37','2021-03-02 17:57:37'),(7,2,'Diez Mil',10000,'2021-03-02 17:57:37','2021-03-02 17:57:37'),(8,2,'Veinte Mil',20000,'2021-03-02 17:57:37','2021-03-02 17:57:37'),(9,2,'Cincueta Mil',50000,'2021-03-02 17:57:37','2021-03-02 17:57:37'),(10,2,'Cien Mil',100000,'2021-03-02 17:57:37','2021-03-02 17:57:37');
/*!40000 ALTER TABLE `dinero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento`
--

DROP TABLE IF EXISTS `movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimiento` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_caja` int DEFAULT NULL,
  `id_tipo_movimiento` int DEFAULT NULL,
  `ingreso` double NOT NULL,
  `egreso` double NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `movimiento_id` (`id`),
  KEY `IDX_C8FF107A6F45E807` (`id_caja`),
  KEY `IDX_C8FF107AB027D1DA` (`id_tipo_movimiento`),
  CONSTRAINT `FK_C8FF107A6F45E807` FOREIGN KEY (`id_caja`) REFERENCES `caja` (`id`),
  CONSTRAINT `FK_C8FF107AB027D1DA` FOREIGN KEY (`id_tipo_movimiento`) REFERENCES `tipo_movimiento` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento`
--

LOCK TABLES `movimiento` WRITE;
/*!40000 ALTER TABLE `movimiento` DISABLE KEYS */;
INSERT INTO `movimiento` VALUES (1,1,3,20000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(2,1,3,20000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(3,1,3,20000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(4,1,3,20000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(5,1,3,20000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(6,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(7,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(8,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(9,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(10,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(11,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(12,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(13,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(14,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(15,2,3,10000,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(16,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(17,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(18,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(19,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(20,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(21,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(22,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(23,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(24,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(25,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(26,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(27,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(28,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(29,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(30,3,3,500,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(31,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(32,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(33,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(34,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(35,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(36,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(37,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(38,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(39,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(40,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(41,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(42,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(43,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(44,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(45,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(46,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(47,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(48,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(49,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23'),(50,4,3,200,0,'2021-03-02 17:58:23','2021-03-02 17:58:23');
/*!40000 ALTER TABLE `movimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_dinero`
--

DROP TABLE IF EXISTS `tipo_dinero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_dinero` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipo_dinero_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_dinero`
--

LOCK TABLES `tipo_dinero` WRITE;
/*!40000 ALTER TABLE `tipo_dinero` DISABLE KEYS */;
INSERT INTO `tipo_dinero` VALUES (1,'Moneda','1','2021-03-02 17:56:53','2021-03-02 17:56:53'),(2,'Billete','2','2021-03-02 17:56:53','2021-03-02 17:56:53');
/*!40000 ALTER TABLE `tipo_dinero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_movimiento`
--

DROP TABLE IF EXISTS `tipo_movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_movimiento` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigo` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipo_movimiento_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_movimiento`
--

LOCK TABLES `tipo_movimiento` WRITE;
/*!40000 ALTER TABLE `tipo_movimiento` DISABLE KEYS */;
INSERT INTO `tipo_movimiento` VALUES (1,'Ingreso',1,'2021-03-02 17:56:53','2021-03-02 17:56:53'),(2,'Egreso',2,'2021-03-02 17:56:53','2021-03-02 17:56:53'),(3,'Carga',3,'2021-03-02 17:56:53','2021-03-02 17:56:53'),(4,'Retiro',4,'2021-03-02 17:56:53','2021-03-02 17:56:53');
/*!40000 ALTER TABLE `tipo_movimiento` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-02 18:01:28
