insert into tipo_dinero (id, nombre, codigo, fecha_creacion, fecha_actualizacion) values (1, 'Moneda', 1, sysdate(), sysdate());
insert into tipo_dinero (id, nombre, codigo, fecha_creacion, fecha_actualizacion) values (2, 'Billete', 2, sysdate(), sysdate());

insert into tipo_movimiento values (1, 'Ingreso', 1, sysdate(), sysdate());
insert into tipo_movimiento values (2, 'Egreso', 2, sysdate(), sysdate());
insert into tipo_movimiento values (3, 'Carga', 3, sysdate(), sysdate());
insert into tipo_movimiento values (4, 'Retiro', 4, sysdate(), sysdate());