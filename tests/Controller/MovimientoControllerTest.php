<?php

// tests/Util/CalculatorTest.php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MovimientoControllerTest extends WebTestCase
{
    public function testGetAll() {
        $client = static::createClient();

        $client->request('GET', '/api/movimientos');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }


    public function testGetEstados() {
        $client = static::createClient();

        $client->request('GET', '/api/movimientos/estado?fechaInicio=2021-03-02 17:58:23&fechaFin=2021-03-02 17:58:23');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}