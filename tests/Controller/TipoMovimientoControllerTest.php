<?php

// tests/Util/CalculatorTest.php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TipoMovimientoControllerTest extends WebTestCase
{
    public function testGetAll() {
        $client = static::createClient();

        $client->request('GET', '/api/tipo/movimientos');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    
}