<?php

// tests/Util/CalculatorTest.php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CajaControllerTest extends WebTestCase
{
    public function testGetAll() {
        $client = static::createClient();

        $client->request('GET', '/api/cajas');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetTotal() {
        $client = static::createClient();

        $client->request('GET', '/api/cajas/total');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testLoadBase() {
        $client = static::createClient();

        $client->request('GET', '/api/caja/base');

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testPay() {
        $client = static::createClient();

        $client->request(
            'POST', 
            '/api/caja/pay?valorTotal=10000&pago=50000',
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testEmptyBase() {
        $client = static::createClient();

        $client->request(
            'GET', 
            '/api/caja/empty',
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    
}