<?php

// tests/Util/CalculatorTest.php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DineroControllerTest extends WebTestCase
{
    public function testGetAll() {
        $client = static::createClient();

        $client->request('GET', '/api/dineros');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateDinero() {
        $client = static::createClient();

        $client->request('GET', '/api/dinero/base');

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }
}