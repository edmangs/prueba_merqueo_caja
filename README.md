# README #

Proyecto realizado con el framework de PHP Symfony versión 4.4

### Como compilar el repositorio ###

* Clonar el proyecto en la carpeta de su preferencia.
* Tener instalado composer en su maquina.
* Ejecutar el siguente comando en la carpeta raíz del proyecto: **composer update**.
* Instalar los archivos publicos con el siguiente comando: **bin/console assets:install**.
* Crear un Vhost en apache para desplegar el proyecto o ejecutar el siguiente comando: **bin/console server:start**, podra ingresar a la siguiente ruta: [127.0.0.1:8000](http://127.0.0.1:8000).
* Montar la base de datos con nombre **caja.sql** adjunta en la carpeta **sql**, que esta en la carpeta raíz del proyecto.
* Al ingresar en la aplicación se presentara la página de inicio del framework, para acceder a la información de los APIS ingrear en la siguiente URL: [127.0.0.1:8000/api/doc](http://127.0.0.1:8000/api/doc).
* Podra ver una serie de apis con su documentación y opción para su uso de forma directa.


### Apis y nombres ###

* **Cargar base a la caja** este api se puede encontrar con el nombre de [/api/caja/base](http://127.0.0.1:8000/api/caja/base).
* **Vaciar caja** este api se puede encontrar con el nombre de [/api/caja/empty](http://127.0.0.1:8000/api/cajas/total).
* **Estado de caja** este api se puede encontrar con el nombre de [/api/movimientos/estado](http://127.0.0.1:8000/api/movimientos/estado?fechaInicio=2021/03/02 00:00:00&fechaFin=2021/03/02 23:59:00).
* **Realizar un pago** este api se puede encontrar con el nombre de [/api/caja/pay](http://127.0.0.1:8000/api/caja/pay?valorTotal=10000&pago=50000).
* **Ver registro de logs de eventos** este api se puede encontrar con el nombre de [/api/movimientos](http://127.0.0.1:8000/api/movimientos).
* **Saber estado de la caja** este api se puede encontrar con el nombre de [Saber estado de la caja](http://127.0.0.1:8000/api/movimientos/estado?fechaInicio=2021/03/02 00:00:00&fechaFin=2021/03/02 23:59:00).

### Pruebas unitarias ###

Se recomienda hacer las pruebas unitarias al finalizar la revisión manual de la aplicación, ya que ejecutarlas implica dejar vacía la caja.

* Las pruebas unitaras se encuentran ubicadas en la ruta **tests\Controller** 
* Para ejecutar las pruebas unitarias hacer los siguietnes pasos: 
	* Ubicarse en la carpeta raíz del pryecto.
	* Ejecutar el siguiente comando desde una terminal de comandos: **php bin/phpunit**
* Se presentara un cuadro con los resultados de la ejecución

### Documentación del proyecto ###

* Se adjunta un modelo MER que esta en la carpeta **documentacion** con el nombre de: **MER_API_MRQUEO.png**
* Se adjunta una modelo de diseño que esta en la carpeta **documentacion** con el nombre de: **Solucion Api Merqueo.png**