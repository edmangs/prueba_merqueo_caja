<?php

namespace App\Repository;

use App\Entity\Caja;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Caja|null find($id, $lockMode = null, $lockVersion = null)
 * @method Caja|null findOneBy(array $criteria, array $orderBy = null)
 * @method Caja[]    findAll()
 * @method Caja[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CajaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Caja::class);
    }

    /**
     * @return Caja[] Returns an array of Caja objects
     */
    public function findCajaActiva() {
        return $this->createQueryBuilder('c')
            ->andWhere('c.cantidad <> 0')
            ->innerJoin('c.dinero', 'd')
            ->orderBy('d.valor', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Caja Returns an array of Caja objects
     */
    public function findCajaCambio($valor) {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.dinero', 'd')
            ->andWhere('d.valor = :valor')
            ->orderBy('d.valor', 'DESC')
            ->setParameter('valor', $valor)
            ->getQuery()
            ->getOneOrNullResult();
    }
    
    /*
    public function findOneBySomeField($value): ?Caja
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
