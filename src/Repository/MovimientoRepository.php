<?php

namespace App\Repository;

use App\Entity\Movimiento;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Movimiento|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movimiento|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movimiento[]    findAll()
 * @method Movimiento[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovimientoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movimiento::class);
    }

    /**
     * @return Movimiento[] Returns an array of Movimiento objects
     */
    public function findMovimientos() {
        return $this->createQueryBuilder('m')
            ->innerJoin('m.caja', 'c')
            ->innerJoin('c.dinero', 'd')
            ->andWhere('c.cantidad <> 0')
            ->andWhere('m.ingreso > 0')
            ->orderBy('d.valor', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }


    /**
     * @return Movimiento[] Returns an array of Movimiento objects
     */
    public function findMovimientosFechas($fechaInicio, $fechaFin) {
        return $this->createQueryBuilder('m')
            ->andWhere('m.fechaCreacion Between :fechaInicio AND :fechaFin')
            ->setParameter('fechaInicio', $fechaInicio)
            ->setParameter('fechaFin', $fechaFin)
            ->getQuery()
            ->getResult()
        ;
    }
    
    /*
    public function findOneBySomeField($value): ?Movimiento
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
