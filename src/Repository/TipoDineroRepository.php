<?php

namespace App\Repository;

use App\Entity\TipoDinero;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TipoDinero|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoDinero|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoDinero[]    findAll()
 * @method TipoDinero[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoDineroRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoDinero::class);
    }

    // /**
    //  * @return TipoDinero[] Returns an array of TipoDinero objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoDinero
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
