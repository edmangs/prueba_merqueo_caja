<?php

namespace App\EventListener;

use App\Entity\Caja;
use App\Entity\Movimiento;
use App\Entity\TipoMovimiento;
use App\Entity\User;
use App\Model\Constantes;
use App\Util\Util;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Psr\Container\ContainerInterface;

class CajaListener
{
    protected $em;
    protected $container;
    protected $util;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->util = New Util();
    }

    public function prePersist(LifecycleEventArgs $event): void {
        $entity = $event->getObject();
        $this->em = $event->getObjectManager();
        
        if($entity instanceof Caja){
            $this->generarMovimientos($entity);
        }
    }

    public function preUpdate(LifecycleEventArgs $event): void {
        $entity = $event->getObject();
        $this->em = $event->getObjectManager();
        
        if($entity instanceof Caja){
            $this->generarMovimientos($entity);
        }
    }

    public function generarMovimientos(Caja $entity){
        $this->util->generarMovimientos($entity, $this->container, $this->em);
    }
}