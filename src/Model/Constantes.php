<?php

namespace App\Model;

class Constantes {

    const MENSAJE = [
        'vacio'         =>      [ 'mensaje' => 'La paticiòn se encuentra vacia' ],
        'existe'        =>      [ 'mensaje' => 'El elemento ya existe' ],
        'dineroBase'    =>      [ 'mensaje' => 'Se genero el dinero base' ],
        'cajaBase'      =>      [ 'mensaje' => 'Se genero la caja base' ],
        'noDinero'      =>      [ 'mensaje' => 'No existe el dinero para generar la base' ],
        'emptyCaja'     =>      [ 'mensaje' => 'La caja ha sido vacia' ],
        'noCaja'        =>      [ 'mensaje' => 'La caja no tiene registros' ],
        'cambioCero'    =>      [ 'mensaje' => 'El cambio de esta compra es 0' ],
        'pagoMenor'     =>      [ 'mensaje' => 'El valor del pago es menor al valor total de la compra' ],
        'noCambioCaja'  =>      [ 'mensaje' => 'la caja no tiene dinero para hacer el cambio' ]
    ];


    const DINERO = [
        [ 'nombre' => 'Cincuenta', 'valor' => 50, "tipo" => ['codigo' => 1]],
        [ 'nombre' => 'Cien', 'valor' => 100 , "tipo" => ['codigo' => 1]],
        [ 'nombre' => 'Dosientos', 'valor' => 200 , "tipo" => ['codigo' => 1]],
        [ 'nombre' => 'Quinientos', 'valor' => 500 , "tipo" => ['codigo' => 1]],
        [ 'nombre' => 'Mil', 'valor' => 1000 , "tipo" => ['codigo' => 2]],
        [ 'nombre' => 'Cinco Mil', 'valor' => 5000 , "tipo" => ['codigo' => 2]],
        [ 'nombre' => 'Diez Mil', 'valor' => 10000 , "tipo" => ['codigo' => 2]],
        [ 'nombre' => 'Veinte Mil', 'valor' => 20000 , "tipo" => ['codigo' => 2]],
        [ 'nombre' => 'Cincueta Mil', 'valor' => 50000 , "tipo" => ['codigo' => 2]],
        [ 'nombre' => 'Cien Mil', 'valor' => 100000 , "tipo" => ['codigo' => 2]],
    ];

    const CAJA = [
        [ 'cantidad' => '5', 'dinero' => 20000],
        [ 'cantidad' => '10', 'dinero' => 10000],
        [ 'cantidad' => '15', 'dinero' => 500],
        [ 'cantidad' => '20', 'dinero' => 200]
    ];

    const TIPO_MOVIMIENTO = [
        'Ingreso'        =>      1,
        'Egreso'         =>      2,
        'Carga'          =>      3,
        'Retiro'         =>      4
    ];

}