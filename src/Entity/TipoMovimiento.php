<?php

namespace App\Entity;

use App\Repository\TipoMovimientoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=TipoMovimientoRepository::class)
 * @ORM\Table(name="tipo_movimiento", uniqueConstraints={@ORM\UniqueConstraint(name="tipo_movimiento_id", columns={"id"})}))
 * @Serializer\ExclusionPolicy("all")
 */
class TipoMovimiento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id")
     * @Serializer\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="nombre")
     * @Serializer\Expose
     */
    private $nombre;

    /**
     * @ORM\Column(type="integer", name="codigo")
     * @Serializer\Expose
     */
    private $codigo;

    /**
     * @ORM\Column(type="datetime", name="fecha_creacion")
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $fechaCrecion;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $FechaActualizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Movimiento", mappedBy="tipoMovimiento")
     */
    private $movimientos;

    public function __construct()
    {
        $this->movimientos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCodigo(): ?int
    {
        return $this->codigo;
    }

    public function setCodigo(int $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getFechaCrecion(): ?\DateTimeInterface
    {
        return $this->fechaCrecion;
    }

    public function setFechaCrecion(\DateTimeInterface $fechaCrecion): self
    {
        $this->fechaCrecion = $fechaCrecion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->FechaActualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $FechaActualizacion): self
    {
        $this->FechaActualizacion = $FechaActualizacion;

        return $this;
    }

    /**
     * @return Collection|Movimiento[]
     */
    public function getMovimientos(): Collection
    {
        return $this->movimientos;
    }

    public function addMovimiento(Movimiento $movimiento): self
    {
        if (!$this->movimientos->contains($movimiento)) {
            $this->movimientos[] = $movimiento;
            $movimiento->setTipoMovimiento($this);
        }

        return $this;
    }

    public function removeMovimiento(Movimiento $movimiento): self
    {
        if ($this->movimientos->removeElement($movimiento)) {
            // set the owning side to null (unless already changed)
            if ($movimiento->getTipoMovimiento() === $this) {
                $movimiento->setTipoMovimiento(null);
            }
        }

        return $this;
    }

}
