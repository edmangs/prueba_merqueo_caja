<?php

namespace App\Entity;

use App\Repository\TipoDineroRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=TipoDineroRepository::class)
 * @ORM\Table(name="tipo_dinero", uniqueConstraints={@ORM\UniqueConstraint(name="tipo_dinero_id", columns={"id"})}))
 * @Serializer\ExclusionPolicy("all")
 */
class TipoDinero
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="nombre")
     * @Serializer\Expose
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, name="codigo")
     * @Serializer\Expose
     */
    private $codigo;

    /**
     * @ORM\Column(type="datetime", name="fecha_creacion")
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="datetime", name="fecha_actualizacion")
     * @Gedmo\Timestampable(on="update")
     * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $fechaActualizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Dinero", mappedBy="tipoDinero")
     * @Serializer\Exclude
     */
    private $dineros;

    public function __construct()
    {
        $this->dineros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    /**
     * @return Collection|Dinero[]
     */
    public function getDineros(): Collection
    {
        return $this->dineros;
    }

    public function addDinero(Dinero $dinero): self
    {
        if (!$this->dineros->contains($dinero)) {
            $this->dineros[] = $dinero;
            $dinero->setTipoDinero($this);
        }

        return $this;
    }

    public function removeDinero(Dinero $dinero): self
    {
        if ($this->dineros->removeElement($dinero)) {
            // set the owning side to null (unless already changed)
            if ($dinero->getTipoDinero() === $this) {
                $dinero->setTipoDinero(null);
            }
        }

        return $this;
    }


}
