<?php

namespace App\Entity;

use App\Repository\CajaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=CajaRepository::class)
 * @ORM\Table(name="caja", uniqueConstraints={@ORM\UniqueConstraint(name="caja_id", columns={"id"})}))
 * @Serializer\ExclusionPolicy("all")
 */
class Caja
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id")
     * @Serializer\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="cantidad")
     * @Serializer\Expose
     */
    private $cantidad;

    /**
     * @ORM\Column(type="float", name="cambio")
     * @Serializer\Expose
     */
    private $cambio;

    /**
     * @ORM\Column(type="datetime", name="fecha_creacion")
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Expose
     * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="datetime", name="fecha_actualizacion")
     * @Gedmo\Timestampable(on="update")
     * @Serializer\Expose
     */
    private $fechaActualizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Dinero", inversedBy="cajas")
     * @ORM\JoinColumn(name="id_dinero", referencedColumnName="id")
     * @Serializer\Expose
     * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $dinero;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Movimiento", mappedBy="caja")
     */
    private $movimientos;


    private $tipoMovimiento;

    //------------------------------------------ Cambios -----------------------------------------------

    /**
     * Get the value of tipoMovimiento
     */ 
    public function getTipoMovimiento()
    {
        return $this->tipoMovimiento;
    }

    /**
     * Set the value of tipoMovimiento
     *
     * @return  self
     */ 
    public function setTipoMovimiento($tipoMovimiento)
    {
        $this->tipoMovimiento = $tipoMovimiento;

        return $this;
    }

    //--------------------------------------------------------------------------------------------------
    

    public function __construct()
    {
        $this->movimientos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getCambio(): ?float
    {
        return $this->cambio;
    }

    public function setCambio(float $cambio): self
    {
        $this->cambio = $cambio;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getDinero(): ?Dinero
    {
        return $this->dinero;
    }

    public function setDinero(?Dinero $dinero): self
    {
        $this->dinero = $dinero;

        return $this;
    }

    /**
     * @return Collection|Movimiento[]
     */
    public function getMovimientos(): Collection
    {
        return $this->movimientos;
    }

    public function addMovimiento(Movimiento $movimiento): self
    {
        if (!$this->movimientos->contains($movimiento)) {
            $this->movimientos[] = $movimiento;
            $movimiento->setCaja($this);
        }

        return $this;
    }

    public function removeMovimiento(Movimiento $movimiento): self
    {
        if ($this->movimientos->removeElement($movimiento)) {
            // set the owning side to null (unless already changed)
            if ($movimiento->getCaja() === $this) {
                $movimiento->setCaja(null);
            }
        }

        return $this;
    }

}
