<?php

namespace App\Entity;

use App\Repository\DineroRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=DineroRepository::class)
 * @ORM\Table(name="dinero", uniqueConstraints={@ORM\UniqueConstraint(name="nombre", columns={"nombre"})}))
 * @Serializer\ExclusionPolicy("all")
 */
class Dinero
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="nombre")
     * @Serializer\Expose
     */
    private $nombre;

    /**
     * @ORM\Column(type="float", name="valor")
     * @Serializer\Expose
     */
    private $valor;

    /**
     * @ORM\Column(type="datetime", name="fecha_creacion")
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="datetime", name="fecha_actualizacion")
     * @Gedmo\Timestampable(on="update")
     * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $fechaActualizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoDinero", inversedBy="dineros", cascade={"persist", "remove" })
     * @ORM\JoinColumn(name="id_tipo_dinero", referencedColumnName="id")
     * @Serializer\Exclude
     */
    private $tipoDinero;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Caja", mappedBy="dinero")
     * @Serializer\Exclude
     */
    private $cajas;

    public function __construct()
    {
        $this->cajas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getValor(): ?float
    {
        return $this->valor;
    }

    public function setValor(float $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getTipoDinero(): ?TipoDinero
    {
        return $this->tipoDinero;
    }

    public function setTipoDinero(?TipoDinero $tipoDinero): self
    {
        $this->tipoDinero = $tipoDinero;

        return $this;
    }

    /**
     * @return Collection|Caja[]
     */
    public function getCajas(): Collection
    {
        return $this->cajas;
    }

    public function addCaja(Caja $caja): self
    {
        if (!$this->cajas->contains($caja)) {
            $this->cajas[] = $caja;
            $caja->setDinero($this);
        }

        return $this;
    }

    public function removeCaja(Caja $caja): self
    {
        if ($this->cajas->removeElement($caja)) {
            // set the owning side to null (unless already changed)
            if ($caja->getDinero() === $this) {
                $caja->setDinero(null);
            }
        }

        return $this;
    }
     
}
