<?php

namespace App\Entity;

use App\Repository\MovimientoRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=MovimientoRepository::class)
 * @ORM\Table(name="movimiento", uniqueConstraints={@ORM\UniqueConstraint(name="movimiento_id", columns={"id"})}))
 * @Serializer\ExclusionPolicy("all")
 */
class Movimiento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id")
     * @Serializer\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="float", name="ingreso")
     * @Serializer\Expose
     */
    private $ingreso;

    /**
     * @ORM\Column(type="float", name="egreso")
     * @Serializer\Expose
     */
    private $egreso;

    /**
     * @ORM\Column(type="datetime", name="fecha_creacion")
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Expose
     * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="datetime", name="fecha_actualizacion")
     * @Gedmo\Timestampable(on="update")
     * @Serializer\Expose
    * @Serializer\Type("DateTime<'Y/m/d h:i', 'America/Colombia'>")
     */
    private $fechaActualizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Caja", inversedBy="movimientos")
     * @ORM\JoinColumn(name="id_caja", referencedColumnName="id")
     */
    private $caja;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoMovimiento", inversedBy="movimientos")
     * @ORM\JoinColumn(name="id_tipo_movimiento", referencedColumnName="id")
     * @Serializer\Expose
     */
    private $tipoMovimiento;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIngreso(): ?float
    {
        return $this->ingreso;
    }

    public function setIngreso(float $ingreso): self
    {
        $this->ingreso = $ingreso;

        return $this;
    }

    public function getEgreso(): ?float
    {
        return $this->egreso;
    }

    public function setEgreso(float $egreso): self
    {
        $this->egreso = $egreso;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getCaja(): ?Caja
    {
        return $this->caja;
    }

    public function setCaja(?Caja $caja): self
    {
        $this->caja = $caja;

        return $this;
    }

    public function getTipoMovimiento(): ?TipoMovimiento
    {
        return $this->tipoMovimiento;
    }

    public function setTipoMovimiento(?TipoMovimiento $tipoMovimiento): self
    {
        $this->tipoMovimiento = $tipoMovimiento;

        return $this;
    }

}
