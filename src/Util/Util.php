<?php

namespace App\Util;

use App\Entity\Caja;
use App\Entity\Movimiento;
use App\Entity\TipoMovimiento;
use App\Model\Constantes;
use Psr\Container\ContainerInterface;

class Util {
    function mejorCambio(Array $dineros, Int $total) {
        $numeroMonedas =count($dineros);
        $cambio = [];
        for($i=0; $i < $numeroMonedas; $i++ ) {
            while($total >= $dineros[$i]) {
                $total= $total - $dineros[$i];
                
                if (!isset($cambio[$dineros[$i]])) {
                    $cambio[$dineros[$i]]  = 0;
                }
                
                $cambio[$dineros[$i]] ++;
            }
        }
        
        return $cambio;
    }

    public function generarMovimientos(Caja $entity, ContainerInterface $container, $em){
        for($i = 0; $i < $entity->getCantidad(); $i++){
            $movimiento = new Movimiento();
            if($entity->getTipoMovimiento() == Constantes::TIPO_MOVIMIENTO['Carga'] || $entity->getTipoMovimiento() == Constantes::TIPO_MOVIMIENTO['Ingreso']){
                $movimiento->setIngreso($entity->getDinero()->getValor());
                $movimiento->setEgreso(0);
            } else {
                $movimiento->setIngreso(0);
                $movimiento->setEgreso($entity->getDinero()->getValor());
            }

            $tipoMovimiento = $container->get('doctrine')->getRepository(TipoMovimiento::class)->findOneBy(['codigo' => $entity->getTipoMovimiento()]);

            $movimiento->setCaja($entity);
            $movimiento->setTipoMovimiento($tipoMovimiento);

            $em->persist($movimiento);
        }
    }
}