<?php

namespace App\Controller;

use App\Entity\Movimiento;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class MovimientoController extends AbstractController
{
    /**
     * Lista de los datos guardados en la entidad Movimiento
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/movimientos", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Retorna los datos de la entidad",
     * )
     * @OA\Tag(name="movimiento")
     */
    public function getAll(Request $request)
    {
        $entities = $this->getDoctrine()
            ->getRepository(Movimiento::class)
            ->findAll();

        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($entities, 'json');

        $response = new Response($jsonContent, 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Lista de los datos guardados en la entidad Movimiento
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/movimientos/estado", methods={"GET"})
     * @OA\Parameter(
     *     name="fechaInicio",
     *     in="query",
     *     description="Fecha de inico de la busqueda - 2021/01/01 00:00",
     *     @OA\Schema(type="date")
     * )
     * @OA\Parameter(
     *     name="fechaFin",
     *     in="query",
     *     description="Fecha fin de la busqueda - 2021/01/01 23:59",
     *     @OA\Schema(type="date")
     * )
     * @OA\Response(
     *     response=200, description="Retorna los datos de la entidad",
     * )
     * @OA\Response(
     *     response=204, description="La peticion esta vacia"
     * ),
     * @OA\Response(
     *     response=201, description="El elemento se creo"
     * ),
     * @OA\Response(
     *     response=400, description="Peticion erronea"
     * ),
     * @OA\Response(
     *     response=404, description="Elemento no encontrado"
     * )
     * @OA\Tag(name="movimiento")
     */
    public function getEstados(Request $request) {
        $fechaInicio = $request->get('fechaInicio', '');
        $fechaFin = $request->get('fechaFin', '');
        
        $entities = $this->getDoctrine()->getRepository(Movimiento::class)->findMovimientosFechas($fechaInicio, $fechaFin);

        
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($entities, 'json');

        $response = new Response($jsonContent, 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
