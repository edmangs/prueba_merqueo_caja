<?php

namespace App\Controller;

use App\Entity\Caja;
use App\Entity\Dinero;
use App\Entity\Movimiento;
use App\Model\Constantes;
use App\Util\Util;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CajaController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    protected $util;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->em = $entityManager;
        $this->util = new Util();
    }

    /**
     * Lista de los datos guardados en la entidad Caja
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/cajas", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Retorna los datos de la entidad",
     * )
     * @OA\Tag(name="caja")
     */
    public function getAll(Request $request) {
        $entities  = $this->getDoctrine()
            ->getRepository(Caja::class)
            ->findAll();

        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($entities, 'json');

        $response = new Response($jsonContent, 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Lista de los datos guardados en la entidad Caja
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/cajas/total", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Retorna los datos de la entidad",
     * )
     * @OA\Tag(name="caja")
     */
    public function getTotal(Request $request) {
        $entities  = $this->getDoctrine()->getRepository(Caja::class)->findAll();
        $total  = 0;

        foreach($entities as $caja){
            $total += $caja->getDinero()->getValor() * $caja->getCantidad();
        }

        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();

        $jsonTotal = [
            'total' => $total,
            'cajas' => $entities
        ];

        $jsonTotal = $serializer->serialize($jsonTotal, 'json');

        $response = new Response($jsonTotal, 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Lista de los datos guardados en la entidad Caja
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/caja/base", methods={"GET"})
     * @OA\Response(
     *     response=200, description="Retorna los datos de la entidad",
     * )
     * @OA\Response(
     *     response=204, description="La peticion esta vacia"
     * ),
     * @OA\Response(
     *     response=201, description="El elemento se creo"
     * ),
     * @OA\Response(
     *     response=400, description="Peticion erronea"
     * ),
     * @OA\Response(
     *     response=404, description="Elemento no encontrado"
     * )
     * @OA\Tag(name="caja")
     */
    public function loadBase(Request $request) {
        foreach (Constantes::CAJA as $caja) {
            $dinero = $this->getDoctrine()->getRepository(Dinero::class)->findOneBy(['valor' => $caja['dinero']]);
            
            if(!$dinero){
                return $this->json(Constantes::MENSAJE['noDinero'], 400);
            }

            
            $elemento = $this->getDoctrine()->getRepository(Caja::class)->findCajaCambio($caja['dinero']);
            if(!$elemento){
                $elemento = new Caja();
            }
            $elemento->setCantidad($caja['cantidad']);
            $elemento->setDinero($dinero);
            $elemento->setCambio(0);
            $elemento->setTipoMovimiento(Constantes::TIPO_MOVIMIENTO['Carga']);

            $this->em->persist($elemento);
        }
        
        $this->em->flush();
        
        return $this->json(Constantes::MENSAJE['cajaBase'], 201);
    }

    /**
     * Lista de los datos guardados en la entidad Caja
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/caja/empty", methods={"GET"})
     * @OA\Response(
     *     response=200, description="Retorna los datos de la entidad",
     * )
     * @OA\Response(
     *     response=204, description="La peticion esta vacia"
     * ),
     * @OA\Response(
     *     response=201, description="El elemento se creo"
     * ),
     * @OA\Response(
     *     response=400, description="Peticion erronea"
     * ),
     * @OA\Response(
     *     response=404, description="Elemento no encontrado"
     * )
     * @OA\Tag(name="caja")
     */
    public function emptyBase(Request $request) {
        $entities  = $this->getDoctrine()
            ->getRepository(Caja::class)
            ->findAll();

        if(!$entities){
            return $this->json(Constantes::MENSAJE['noCaja'], 400);
        }

        foreach ($entities as $caja) {
            $caja->setTipoMovimiento(Constantes::TIPO_MOVIMIENTO['Retiro']);
            $this->util->generarMovimientos($caja, $this->container, $this->em);
            
            $caja->setCantidad(0);
            
            $this->em->persist($caja);
        }
        
        $this->em->flush();
        
        return $this->json(Constantes::MENSAJE['emptyCaja'], 200);
    }

    /**
     * Lista de los datos guardados en la entidad Caja
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/caja/pay", methods={"POST"})
     * @OA\Parameter(
     *     name="valorTotal",
     *     in="query",
     *     description="Valor total del producto",
     *     @OA\Schema(type="number")
     * )
     * @OA\Parameter(
     *     name="pago",
     *     in="query",
     *     description="Dinero entregado por el cliente",
     *     @OA\Schema(type="number")
     * )
     * @OA\Response(
     *     response=200, description="Retorna los datos de la entidad",
     * )
     * @OA\Response(
     *     response=204, description="La peticion esta vacia"
     * ),
     * @OA\Response(
     *     response=201, description="El elemento se creo"
     * ),
     * @OA\Response(
     *     response=400, description="Peticion erronea"
     * ),
     * @OA\Response(
     *     response=404, description="Elemento no encontrado"
     * )
     * @OA\Tag(name="caja")
     */
    public function pay(Request $request) {
        $valorTotal = $request->get('valorTotal', 0);
        $pago = $request->get('pago', 0);

        if($valorTotal == $pago){
            return $this->json(Constantes::MENSAJE['cambioCero'], 200);
        }

        if($valorTotal > $pago){
            return $this->json(Constantes::MENSAJE['pagoMenor'], 400);
        }
        
        $dinero = $this->getDoctrine()->getRepository(Dinero::class)->findOneBy(['valor' => $pago]);
        if(!$dinero){
            return $this->json(Constantes::MENSAJE['noDinero'], 400);
        }
        

        if(!$dinero){
            return $this->json(Constantes::MENSAJE['noDinero'], 400);
        }

        $cajas = $this->getDoctrine()->getRepository(Caja::class)->findCajaActiva();
        
        $cambios = [];
        foreach ($cajas as $caja) {
            for ($i = 0; $i < $caja->getCantidad(); $i++) {
                $cambios[] = $caja->getDinero()->getValor();
            }
        }
        
        $cambios = $this->util->mejorCambio($cambios, ($pago-$valorTotal));

        if(count($cambios) <= 0){
            return $this->json(Constantes::MENSAJE['noCambioCaja'], 400);
        }

        $caja = $this->getDoctrine()->getRepository(Caja::class)->findCajaCambio($pago);
        if(!$caja){
            $caja =  new Caja();
            $caja->setCantidad(1);
            $caja->setDinero($dinero);
            $caja->setCambio(0);
            $caja->setTipoMovimiento(Constantes::TIPO_MOVIMIENTO['Ingreso']);
        } else {
            $cantidad = $caja->getCantidad();
            $caja->setTipoMovimiento(Constantes::TIPO_MOVIMIENTO['Ingreso']);
            $caja->setCantidad(1);
            $this->util->generarMovimientos($caja, $this->container, $this->em);
            $caja->setCantidad($cantidad + 1);
        }

        $this->em->persist($caja);

        foreach ($cambios as $key => $cambio) {
            $caja = $this->getDoctrine()->getRepository(Caja::class)->findCajaCambio($key);
            if($caja){
                $cantidad = $caja->getCantidad();
                $caja->setTipoMovimiento(Constantes::TIPO_MOVIMIENTO['Egreso']);
                $caja->setCantidad($cambio);
                $this->util->generarMovimientos($caja, $this->container, $this->em);

                $caja->setCantidad($cantidad - $cambio);
            }

            $this->em->persist($caja);
        }

        $this->em->flush();

        $jsonResponse = [
            'totalCambio' => ($pago - $valorTotal),
            'cambio'  => $cambios
        ];
        
        return $this->json($jsonResponse, 200);
    }

}
