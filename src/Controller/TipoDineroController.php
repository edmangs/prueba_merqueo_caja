<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use App\Entity\TipoDinero;
use App\Entity\TipoMovimiento;
use App\Model\Constantes;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class TipoDineroController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->em = $entityManager;
    }

    /**
     * Lista de los datos guardados en la entidad TipoDinero
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/tipo/dineros", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Retorna los datos de la entidad",
     * )
     * @OA\Tag(name="tipoDinero")
     */
    public function getAll(Request $request)
    {
        $entities = $this->getDoctrine()
            ->getRepository(TipoDinero::class)
            ->findAll();

        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($entities, 'json');

        $response = new Response($jsonContent, 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Lista de los datos guardados en la entidad TipoDinero
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/tipo/dinero/create", methods={"POST"})
     * @OA\Response(
     *     response=200, description="Retorna los datos de la entidad",
     * )
     * @OA\Response(
     *     response=204, description="La peticion esta vacia"
     * ),
     * @OA\Response(
     *     response=201, description="El elememto se creo"
     * ),
     * @OA\Response(
     *     response=400, description="Peticion erronea"
     * ),
     * @OA\Response(
     *     response=404, description="Elemento no encontrado"
     * ),
     * @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *         @OA\Property(
     *            type = "object", property = "tipoDinero", ref = @Model( type = TipoDinero::class )
     *         ),
     *       ),
     *     ),
     * ),
     * @OA\Tag(name="tipoDinero")
     */
    public function create(Request $request)
    {
        $data = json_decode($request->getContent(), true);  

        if(!$data){
            return $this->json(Constantes::MENSAJE['vacio'], 204);
        }

        $data = $data['tipoDinero'];
        
        $entity = $this->getDoctrine()->getRepository(TipoDinero::class)->findOneBy(['codigo' => $data['codigo']]);

        if($entity){
            return $this->json(Constantes::MENSAJE['existe'], 400);
        }

        $elemento = new TipoDinero();
        $elemento->setNombre($data['nombre']);
        $elemento->setCodigo($data['codigo']);

        $this->em->persist($elemento);
        $this->em->flush();
        
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($elemento, 'json');

        $response = new Response($jsonContent, 201);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
