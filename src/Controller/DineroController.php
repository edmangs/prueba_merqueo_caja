<?php

namespace App\Controller;

use App\Entity\Dinero;
use App\Entity\TipoDinero;
use App\Model\Constantes;
use App\Model\Mensajes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Psr\Container\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

class DineroController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->em = $entityManager;
    }

    /**
     * Lista de los datos guardados en la entidad Dinero
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/dineros", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Retorna los datos de la entidad"
     * )
     * @OA\Tag(name="dinero")
     */
    public function getAll(Request $request, SerializerInterface $serializer)
    {
        $entities = $this->getDoctrine()
            ->getRepository(Dinero::class)
            ->findAll();

        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($entities, 'json');
        
        return new Response($jsonContent);
    }

    /**
     * Lista de los datos guardados en la entidad Dinero
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/dinero/create", methods={"POST"})
     * @OA\Response(
     *     response=200, description="Retorna los datos de la entidad",
     * )
     * @OA\Response(
     *     response=204, description="La peticion esta vacia"
     * ),
     * @OA\Response(
     *     response=201, description="El elemento se creo"
     * ),
     * @OA\Response(
     *     response=400, description="Peticion erronea"
     * ),
     * @OA\Response(
     *     response=404, description="Elemento no encontrado"
     * ),
     * @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *         @OA\Property(
     *            type = "object", property = "dinero", ref = @Model( type = Dinero::class )
     *         ),
     *       ),
     *     ),
     * ),
     * @OA\Tag(name="dinero")
     */
    public function create(Request $request)
    {
        $data = json_decode($request->getContent(), true);  

        if(!$data){
            return $this->json(Constantes::MENSAJE['vacio'], 204);
        }

        $data = $data['dinero'];
        
        $entity = $this->getDoctrine()->getRepository(Dinero::class)->findOneBy(['valor' => $data['valor']]);

        if($entity){
            return $this->json(Constantes::MENSAJE['existe'], 400);
        }

        $elemento = new Dinero();
        $elemento->setNombre($data['nombre']);
        $elemento->setValor($data['valor']);

        $this->em->persist($elemento);
        $this->em->flush();
        
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($elemento, 'json');

        $response = new Response($jsonContent, 201);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Lista de los datos guardados en la entidad Dinero
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/dinero/base", methods={"GET"})
     * @OA\Response(
     *     response=200, description="Retorna los datos de la entidad",
     * )
     * @OA\Response(
     *     response=204, description="La peticion esta vacia"
     * ),
     * @OA\Response(
     *     response=201, description="El elemento se creo"
     * ),
     * @OA\Response(
     *     response=400, description="Peticion erronea"
     * ),
     * @OA\Response(
     *     response=404, description="Elemento no encontrado"
     * )
     * @OA\Tag(name="dinero")
     */
    public function createDinero(Request $request)
    {
        foreach (Constantes::DINERO as $dinero) {
            $elemento = $this->getDoctrine()->getRepository(Dinero::class)->findOneBy(['valor' => $dinero['valor']]);
            if(!$elemento){
                $elemento = new Dinero();
            }

            $tipoDinero = $this->getDoctrine()->getRepository(TipoDinero::class)->findOneBy(['codigo' => $dinero['tipo']['codigo']]);
            if(!$tipoDinero){
                $tipoDinero = new TipoDinero();
            }
            
            $elemento->setNombre($dinero['nombre']);
            $elemento->setValor($dinero['valor']);
            $elemento->setTipoDinero($tipoDinero);

            $this->em->persist($elemento);
        }
        
        $this->em->flush();
        
        return $this->json(Constantes::MENSAJE['dineroBase'], 201);
    }
}
