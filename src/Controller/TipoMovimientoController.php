<?php

namespace App\Controller;

use App\Entity\TipoMovimiento;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class TipoMovimientoController extends AbstractController
{
    /**
     * Lista de los datos guardados en la entidad TipoMovimiento
     *
     * Este llamado presenta todos los datos que tiene guardado la entidad
     *
     * @Route("/api/tipo/movimientos", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Retorna los datos de la entidad",
     * )
     * @OA\Tag(name="tipoMovimiento")
     */
    public function getAll(Request $request)
    {
        $entities = $this->getDoctrine()
            ->getRepository(TipoMovimiento::class)
            ->findAll();

        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($entities, 'json');

        $response = new Response($jsonContent, 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
